import pandas, datetime, json, re
import numpy as np
from collections import defaultdict
reservations_ = pandas.read_csv('raw_data/reservations.csv')
reservations_['created_at'] = reservations_.created_at.apply(lambda dt: datetime.datetime.strptime(str(dt), '%Y-%m-%d %H:%M:%S.%f'))
all_legs_with_routes = pandas.read_excel('processed_data/all_legs_with_routes.xlsx')
reservations_ = reservations_[reservations_.event_id.isin(all_legs_with_routes.event_id.unique().tolist())]
all_rides_legs_final = all_legs_with_routes[['event_id', 'leg_id', 'is_public', 'bus_size', 'riders_final', 'go_live', 'went_live']]
reservations_ = reservations_.merge(all_legs_with_routes[['event_id', 'leg_id']], on='event_id', how='right')
all_routes = all_legs_with_routes[['leg_id', 'created_at','start_time']].drop_duplicates(subset='leg_id')

oneday = datetime.timedelta(days=1)

beginning = datetime.datetime(2015, 5, 24)

print('creating day frames...')
day_frames_dict = dict()
for day_frame in range(277):
    df_datetime = beginning + day_frame * oneday
    df_dict = dict()
    for route_leg_id in all_routes[(all_routes.created_at <= df_datetime) & (all_routes.start_time + oneday >= df_datetime)]['leg_id']:
        #check if it's a fully paid route
        dta = all_rides_legs_final[all_rides_legs_final.leg_id == route_leg_id]
        if dta.go_live.tolist()[0] == 0:
            df_dict[route_leg_id] = dta.bus_size.tolist()[0]
        else:
            df_dict[route_leg_id] = len(reservations_[(reservations_.leg_id == route_leg_id) & (reservations_.created_at <= df_datetime + oneday)])
    day_frames_dict[day_frame] = df_dict.copy()
def is_starting_today(frame, leg_id):
    frame_date = beginning + oneday*frame
    start_d = all_routes[all_routes.leg_id == leg_id].start_time.tolist()[0]
    if abs(frame_date - start_d) < oneday:
        if all_rides_legs_final[all_rides_legs_final.leg_id == leg_id].went_live.tolist()[0] == True:
            return 'success'
        else:
            return 'disappear'
    else: 
        return False

def is_going_live(frame, leg_id):
    #did I go live at all?
    leg_info = all_rides_legs_final[all_rides_legs_final.leg_id == leg_id]
    if leg_info.went_live.tolist()[0] == True:
        #did it happen today?
        golive = leg_info.go_live.tolist()[0]
        res_today = day_frames_dict[frame][leg_id]
        res_yesterday = day_frames_dict[frame-1][leg_id]
        try:
            if res_today / float(go_live) >= 1:
                if res_yesterday / float(go_live) < 1:
                    return 'golive'
        except:
            return False
    return False

def is_going_red_to_yellow(frame, leg_id):
    #did I go live at all?
    leg_info = all_rides_legs_final[all_rides_legs_final.leg_id == leg_id]
    if leg_info.went_live.tolist()[0] == True:
        #did it happen today?
        golive = leg_info.go_live.tolist()[0]
        res_today = day_frames_dict[frame][leg_id]
        res_yesterday = day_frames_dict[frame-1][leg_id]
        try:
            if res_today / float(go_live) >= 0.5:
                if res_yesterday / float(go_live) < 0.5:
                    return 'goyellow'
        except:
            return False
    return False
    
print('creating animation data...')
day_frames_animations = dict()
day_frames_animations[0] = defaultdict(int)
for frame_no in range(1, len(day_frames_dict)): # don't bother with the first day
    day_frame = day_frames_dict[frame_no]
    dfa_dict = defaultdict(int)
    for route_leg_id in day_frame:
        #am I happening today?
        ist = is_starting_today(frame_no, route_leg_id)
        if ist != False:
            dfa_dict[route_leg_id] = ist
            continue
        #did I just appear?
        try:
            #was I there the previous day?
            res_yesterday = day_frames_dict[frame_no-1][route_leg_id]
        except:
            #keyError: Not? Means I appeared
            dfa_dict[route_leg_id] = 'appear'
            continue
        #am I going live?
        igl = is_going_live(frame_no, route_leg_id)
        if igl != False:
            dfa_dict[route_leg_id] = igl
            continue
        # red to yellow? not doing that
#         igrty = is_going_red_to_yellow(frame_no, route_leg_id)
#         if igrty != False:
#             dfa_dict[route_leg_id] = igrty
#             continue
        # no animation
        dfa_dict[route_leg_id] = 0
    day_frames_animations[frame_no] = dfa_dict.copy()


print('creating day frames...')
full_day_frames_dict = dict()

for day_frame in range(277):
    df_list = list()
    for route_leg_id in day_frames_dict[day_frame].keys():
        r_n = day_frames_dict[day_frame][route_leg_id]
        try:
            full_cap, live_thr, publ = [int(x) for x in \
                all_rides_legs_final[all_rides_legs_final.leg_id == route_leg_id]
                                        [['bus_size', 'go_live', 'is_public']].as_matrix()[0]]
            r_n / full_cap
        except:
            continue
        
        try:
            p_t_l = min(r_n / live_thr, 1)
        except:
            p_t_l = 1
            
        if p_t_l < 0.5:
            color = 'red'
        elif p_t_l < 1:
            color = 'yellow'
        else:
            color = 'green'
            
        newroute = { 'leg_id' : route_leg_id, 'reservations_now' : r_n,
                     'progress_to_live' : np.round(p_t_l, 2),
                     'percent_of_full' : np.round(r_n / full_cap, 2),
                     'live_color' : color, 
                     'publ' : publ,
                     'anim' : day_frames_animations[day_frame][route_leg_id]
                    }
        df_list.append(newroute)
    full_day_frames_dict[day_frame] = df_list[:]

all_legs_HQ_polylines = pandas.read_excel('processed_data/all_legs_with_routes_hq_polylines.xlsx')
p = re.compile(r'[A-Za-z- ]+, [A-Z]{2}')
def create_polyline_geojson_feature(leg_data, route_info):
    try:
        a = re.search(p, route_info['start_address_f'].tolist()[0]).group().strip() # get only City, ST
    except:
        a = route_info['start_address_f'].tolist()[0]
    try:
        b = re.search(p, route_info['end_address_f'].tolist()[0]).group().strip()
    except:
        b = route_info['start_address_f'].tolist()[0]
        
    title = a + ' to ' + b
        
    desc = {
        'fr' : route_info['start_address_f'].tolist()[0],
        'to' : route_info['end_address_f'].tolist()[0],
        'dst' : route_info['dist_text'].tolist()[0],
        'eta' : route_info['duration_text'].tolist()[0],
        'stt' : str(route_info['start_time'].tolist()[0]),
        'res' : str(leg_data['reservations_now']), 
        'cap' : str(route_info['bus_size'].tolist()[0])
    }
    ft = '{ "type": "Feature", ' + \
            '"geometry": ' + route_info['geometry_line'].tolist()[0] + ', ' + \
            '"properties": {' + \
                '"title" : "' + title + '",' + \
                '"p_o_f" : ' + str(leg_data['percent_of_full']) + ',' + \
                '"leg_id" : ' + str(leg_data['leg_id']) + ',' + \
                '"publ" : "' + str(leg_data['publ']) + '",' + \
                '"anim" : "' + str(leg_data['anim']) + '",' + \
                '"desc" : '+ json.dumps(desc) +',' + \
                '"color" : "' + leg_data['live_color'] + '"}}'
    return ft

def create_marker_geojson_feature(leg_data, route_info):
    tt = str(leg_data['reservations_now']) + ' riders'
    if leg_data['live_color'] == 'green': 
        tt = 'Live: ' + tt
        tt.replace(' riders', '/'+str(route_info['bus_size'].tolist()[0]) +' riders')
    desc = {
        'fr' : route_info['start_address_f'].tolist()[0],
        'to' : route_info['end_address_f'].tolist()[0],
        'dst' : route_info['dist_text'].tolist()[0],
        'eta' : route_info['duration_text'].tolist()[0],
        'stt' : str(route_info['start_time'].tolist()[0]),
        'res' : str(leg_data['reservations_now']), 
        'cap' : str(route_info['bus_size'].tolist()[0])
    }
    ft_start = '{ "type": "Feature",' + \
            '"geometry": ' + route_info['geometry_marker_s'].tolist()[0] + ', ' + \
            '"properties": {' + \
                '"p_o_f" : ' + str(leg_data['percent_of_full']) + ', ' + \
                '"leg_id" : ' + str(leg_data['leg_id']) + ', ' + \
                '"color" : "' + leg_data['live_color'] + '", ' + \
                '"title" : "' + tt + '",' + \
                '"publ" : "' + str(leg_data['publ']) + '",' + \
                '"anim" : "' + str(leg_data['anim']) + '",' + \
                '"desc" : '+ json.dumps(desc) +',' + \
                '"pos": 0}}'
    ft_end = '{ "type": "Feature",' + \
            '"geometry": ' + route_info['geometry_marker_e'].tolist()[0] + ', ' + \
            '"properties": {' + \
                '"p_o_f" : ' + str(leg_data['percent_of_full']) + ', ' + \
                '"leg_id" : ' + str(leg_data['leg_id']) + ', ' + \
                '"color" : "' + leg_data['live_color'] + '", ' + \
                '"title" : "' + tt + '",' + \
                '"publ" : "' + str(leg_data['publ']) + '",' + \
                '"anim" : "' + str(leg_data['anim']) + '",' + \
                '"desc" : '+ json.dumps(desc) +',' + \
                '"pos" : 1}}'
    return [ft_start, ft_end]

def create_geojson_frame(frame_no, frame_data):
    line_list = list()
    marker_list = list()
    for leg in frame_data:
        route_info = all_legs_with_routes[all_legs_with_routes.leg_id == leg['leg_id']]
        line_list.append(create_polyline_geojson_feature(leg, route_info))
        marker_list.extend(create_marker_geojson_feature(leg, route_info))
    return '{ "type": "FeatureCollection", "features": [' + ', '.join(line_list) + ']}', \
                '{ "type": "FeatureCollection", "features": [' + ', '.join(marker_list) + ']}'
def create_HQ_geojson_frame(frame_no, frame_data):
    line_list = list()
    marker_list = list()
    for leg in frame_data:
        route_info = all_legs_HQ_polylines[all_legs_HQ_polylines.leg_id == leg['leg_id']]
        line_list.append(create_polyline_geojson_feature(leg, route_info))
    return '{ "type": "FeatureCollection", "features": [' + ', '.join(line_list) + ']}'

from pymongo import MongoClient
mcl = MongoClient('mongodb://127.0.0.1:3001/')
db = mcl.meteor

lines_coll = db.geojson_lines 
linesHQ_coll = db.geojson_linesHQ
markers_coll = db.geojson_markers

linesHQ_coll.delete_many({});
lines_coll.delete_many({});
markers_coll.delete_many({});

print('sending data to mongo ...')
gjf = [create_geojson_frame(frm, full_day_frames_dict[frm]) for frm in full_day_frames_dict.keys()]
lines_coll.insert([{'frame' : frm, 'gj' : gj[0]} for frm, gj in enumerate(gjf)])
markers_coll.insert([{'frame' : frm, 'gj' : gj[1]} for frm, gj in enumerate(gjf)])
linesHQ_coll.insert([{'frame' : f, 'gj' : create_HQ_geojson_frame(f, full_day_frames_dict[f])} for f in full_day_frames_dict.keys()])
print("SUCCESS")