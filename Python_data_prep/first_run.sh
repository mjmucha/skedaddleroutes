#!/bin/bash
curl https://install.meteor.com/ | sh
pip3 install -r requirements.txt
cd ..
meteor > /dev/null & APP_PID=$!
echo meteor db started on PID $APP_PID
cd Python_data_prep && python3 generate_database.py &
wait $!
kill -9 $APP_PID