var MUCHA_ACCESS_TOKEN = 'pk.eyJ1IjoibWljaGFtdWNoYSIsImEiOiJjaWwwYjNwcXIxc2M1dXltM3prMDM1MGE4In0.ML08Pn6KFwrEV1S8BBceng';
var MIN_FRAME = 32, MAX_FRAME = 276;
var map, lines_src, markers_src;
var animation_layers = {'0': [], 'appear': [], 'disappear': [], 'success': [], 'golive': []};
var color_layers = {'red': [], 'yellow': [], 'green': []};
var click_layers = [];

MarkerFrames = new Mongo.Collection('geojson_markers');
LineFrames = new Mongo.Collection('geojson_lines');
HQLineFrames = new Mongo.Collection('geojson_linesHQ');

if (Meteor.isClient) {
    // counter starts at 0
    Session.setDefault('tml_value', 55);
    Session.setDefault('frame', 55);
    Session.setDefault('HQ', false);
    Session.setDefault('playpause', false);
    Session.setDefault('layer_vis', {'red': true, 'yellow': true, 'green': true});
    Session.setDefault('route_info', false);
    Session.setDefault('invisible_layers', []);
    Session.setDefault('modalactive', false);

    var datahandle1 = Meteor.subscribe('geojson_lines'),
        datahandle2 = Meteor.subscribe('geojson_markers'),
        datahandle3 = Meteor.subscribe('geojson_linesHQ');

    Template.body.helpers({
        loading: function () {
            return !(datahandle1.ready() && datahandle2.ready());
        }
    });


    Mapbox.debug = true;
    Mapbox.load({
        gl: true
    });

    //Template.Map.onRendered(function () {
    //  this.autorun(function () {
    //    if (Mapbox.loaded()) {
    //      L.mapbox.accessToken = MUCHA_ACCESS_TOKEN;
    //      var map = L.mapbox.map('map', 'mapbox.light');
    //    }
    //  });
    //});
    Template.Map.onRendered(function () {
        this.autorun(function () {
            if (Mapbox.loaded()) {
                mapboxgl.accessToken = MUCHA_ACCESS_TOKEN;
                map = new mapboxgl.Map({
                    container: 'map',
                    attributionControl: false,
                    style: 'mapbox://styles/michamucha/cil1ijb4l00a19klz1vand53o',
                    center: [-72.47, 41.47],
                    zoom: 7.1
                });

                map.on('style.load', function () {
                    // set up data sources
                    markers_src = new mapboxgl.GeoJSONSource({clusters: true});
                    map.addSource('markers', markers_src);
                    lines_src = new mapboxgl.GeoJSONSource();
                    map.addSource('lines', lines_src);
                    map.batch(function (batch) {
                        // set up layers
                        for (var kolor in colorscale) {   // color dimension
                            if (typeof colorscale[kolor] !== 'function') {
                                anim_list.forEach(function (anim) { // animation type
                                    var lines_id = "route." + kolor + '.' + anim;
                                    batch.addLayer({
                                        "id": lines_id,
                                        "type": "line",
                                        "interactive": true,
                                        "source": "lines",
                                        "layout": {
                                            "line-join": "bevel",
                                            "line-cap": "butt"
                                        },
                                        "paint": {
                                            "line-color": colorscale[kolor],
                                            "line-width": linewidth_scale[kolor],
                                            "line-opacity": (anim == '0' ? opacity_scale[kolor] : opacity_anim_scale[anim]),
                                            "line-dasharray": dasharray_anim[anim],
                                        },
                                        "filter": [
                                            'all',
                                            ["==", "color", kolor],
                                            ["==", "anim", anim]]
                                    });
                                    var markers_id = "marker." + kolor + '.' + anim;
                                    batch.addLayer({
                                        "id": markers_id,
                                        "interactive": true,
                                        "type": "circle",
                                        "source": "markers",
                                        "paint": {
                                            "circle-allow-overlap": true,
                                            "circle-color": colorscale[kolor],
                                            "circle-opacity": (anim == '0' ? opacity_scale[kolor] : opacity_anim_scale[anim]),
                                            "circle-radius": linewidth_scale[kolor] // Nice radius value
                                        },
                                        "filter": [
                                            'all',
                                            ["==", "color", kolor],
                                            ["==", "anim", anim]
                                        ]
                                    });
                                    animation_layers[anim].push(lines_id);
                                    animation_layers[anim].push(markers_id);
                                    color_layers[kolor].push(lines_id);
                                    color_layers[kolor].push(markers_id);
                                    click_layers.push(lines_id);
                                    click_layers.push(markers_id);

                                    if (kolor != 'red') {
                                        var lsymbols_id = "llbl." + kolor + '.' + anim;
                                        batch.addLayer({
                                            "id": lsymbols_id,
                                            "type": "symbol",
                                            "interactive": false,
                                            "source": "lines",
                                            "minzoom": 8,
                                            "layout": {
                                                "symbol-placement": "line",
                                                "text-allow-overlap": false,
                                                "text-field": "{title}",
                                                "text-font": ["HelveticaNeue-Medium Medium", "DIN Offc Pro Light"],
                                                "text-max-size": linewidth_scale[kolor],
                                                "symbol-avoid-edges": true,
                                                "text-padding": 2
                                            },
                                            "paint": {
                                                "text-color": "#666666"
                                            },
                                            "filter": [
                                                'all',
                                                ["==", "color", kolor],
                                                ["==", "anim", anim]
                                            ]
                                        });
                                        var msymbols_id = "mlbl." + kolor + '.' + anim;
                                        batch.addLayer({
                                            "id": msymbols_id,
                                            "type": "symbol",
                                            "interactive": false,
                                            "source": "markers",
                                            "minzoom": 7.2,
                                            "layout": {
                                                "text-allow-overlap": false,
                                                "text-field": "{title}",
                                                "text-font": ["HelveticaNeue-Medium Medium", "DIN Offc Pro Light"],
                                                "text-anchor": "center",
                                                "text-max-size": linewidth_scale[kolor] * 2,
                                                "symbol-avoid-edges": true,
                                                "text-padding": 2
                                            },
                                            "paint": {
                                                "text-color": "#666666"
                                            },
                                            "filter": [
                                                'all',
                                                ["==", "color", kolor],
                                                ["==", "anim", anim]
                                            ]
                                        });
                                        animation_layers[anim].push(msymbols_id);
                                        animation_layers[anim].push(lsymbols_id);
                                        color_layers[kolor].push(msymbols_id);
                                        color_layers[kolor].push(lsymbols_id);
                                    }
                                });
                            }
                        }
                    });
                    update_frame();
                });

                map.on('mousemove', function (e) {
                    map.featuresAt(e.point, {
                        radius: 12,
                        layer: click_layers.reverse()
                    }, function (err, features) {
                        if (err || !features.length) {
                            setTimeout(unset_route_info(), 400);
                            return;
                        }
                        var feature = features[0];
                        //console.log(feature);
                        set_route_info(feature.properties.desc, feature.properties.color, feature.properties.publ);
                        map.getCanvas().style.cursor = (!err && features.length) ? 'pointer' : '';
                    });
                });
                //map.on('mousemove', function (e) {
                //    map.featuresAt(e.point, {
                //        radius: 10,
                //        layer: click_layers
                //    }, function (err, features) {
                //        map.getCanvas().style.cursor = (!err && features.length) ? 'pointer' : '';
                //    });
                //});
            }
        });
    });


    Template.Infobox.helpers({
        counter: function () {
            return Session.get('frame');
        },
        HQ: function () {
            return (Session.get('HQ') ? 'checked' : '');
        },
        HQ_not_ready: function () {
            return (!datahandle3.ready() ? 'disabled' : '');
        },
        'playpause': function () {
            return (Session.get('playpause') ? 'pause' : 'play');
        },
        'playing': function () {
            return (Session.get('playpause') ? 'playing' : '');
        },
        red_on: function () {
            return (Session.get('layer_vis')['red'] ? 'checked' : '');
        },
        yellow_on: function () {
            return (Session.get('layer_vis')['yellow'] ? 'checked' : '');
        },
        green_on: function () {
            return (Session.get('layer_vis')['green'] ? 'checked' : '');
        },
        route_info: function () {
            return (Session.get('route_info') != false ? Session.get('route_info') : '');
        },
        ri_visible: function () {
            return (Session.get('route_info') != false ? ' dsp' : '');
        }
    });

    Template.Infobox.events({
        'change #myhqsw': function () {
            Session.set('HQ', !Session.get('HQ'));
            update_lines();
        },
        'change #myredsw, change #myyellowsw, change #mygreensw': function (event) {
            var lv = Session.get('layer_vis');
            var color = event.target.name;
            lv[color] = !lv[color];
            toggle_color(color, lv[color]);
            Session.set('layer_vis', lv);
        },
        'click button.resetview': function () {
            map.flyTo({center: [-72.47, 41.47], zoom: 7.1});
        },
        'click button.moreinfo': function () {
            Session.set('modalactive', !Session.get('modalactive'));
        },
        'click button.playpause': function () {
            Session.set('playpause', !Session.get('playpause'));
            if (Session.get('playpause')) {
                next_frame();
                update_frame();
            }
        },
        'click button.next': function () {
            if (Session.get('frame') != MAX_FRAME) {
                next_frame();
                update_frame();
            }
        },
        'click button.previous': function () {
            if (Session.get('frame') != MIN_FRAME) {
                previous_frame();
                update_frame();
            }
        }

    });

    Template.Map.helpers({
        'frame_date': function () {
            var dt = new Date(2015, 4, 23); // months start from 0, so 4 is May
            dt.setDate(dt.getDate() + Session.get('frame'));
            return dt.toDateString().toLowerCase();
        }
    });

    Template.Timeline.onRendered(function () {
        this.autorun(function () {
            updateTmlOutput(Session.get('frame'), false);
        })
    });
    Template.Timeline.helpers({
        min_frame: function () {
            return MIN_FRAME;
        },
        tml_value: function () {
            return Session.get('frame');
        },
        max_frame: function () {
            return MAX_FRAME;
        },
        rn: function () {
            var rn = "";
            for (var i = MIN_FRAME; i <= MAX_FRAME; i++)
                rn += "<span>" + date_from_frame(i) + "</span>";
            return rn;
        }
    });

    Template.Timeline.events({
        'input input#tml_range': function (event) {
            updateTmlOutput(event.target.value, true);
        },
        'change input#tml_range, mouseup input#tml_range': function (event) {
            //console.log('ONCHANGE:' +Math.round(event.target.value));
            Session.set('frame', Math.round(event.target.value));
            tml_deactivate();
            update_frame();
        }
    });

    Template.Infomodal.helpers({
        modalactive : function () {
            return (Session.get('modalactive') != false ? ' modal_active' : '');
        }
    });
    Template.Infomodal.events({
        'click button.close': function () {
            Session.set('modalactive', !Session.get('modalactive'));
        },
    });
}

if (Meteor.isServer) {
    Meteor.startup(function () {
        // code to run on server at startup
    });
    Meteor.publish("geojson_lines", function () {
        return LineFrames.find();
    });
    Meteor.publish("geojson_linesHQ", function () {
        return HQLineFrames.find();
    });
    Meteor.publish("geojson_markers", function () {
        return MarkerFrames.find();
    });
}

function update_frame() {
    // animate timeline
    updateTmlOutput(Session.get('frame'), false);
    // prep lines for anim
    //prepare_before_next_slide(); nothing to prepare so far
    // get data
    var query = {'frame': Session.get('frame')};
    var lfr = (Session.get('HQ') ? HQLineFrames.findOne(query)
        : LineFrames.findOne(query));
    var mfr = MarkerFrames.findOne(query);
    lines_src.setData(JSON.parse(lfr.gj)); //one for lines, one for markers with labels.
    markers_src.setData(JSON.parse(mfr.gj));

    // animate in appearing routes
    // animate out disappearing unsuccessful routes
    // animate celebration of successful routes
    // animate out successful routes
    run_animations();

    // Play logic
    if (Session.get("playpause") == true) {
        if (Session.get('frame') == MAX_FRAME) {
            Session.set('playpause', !Session.get('playpause'));
        }
        else {
            setTimeout(function () {
                // check if pause was not pressed in the meantime
                if (Session.get('playpause') == false) {
                    setTimeout(function () { restore_defaults_after_animations(); }, 300);
                    return;
                }
                //
                next_frame();
                requestAnimationFrame(update_frame);
            }, 550);
        }
    }

    // catch the situation when mongo collection isn't ready on init
    //if (lfr):

}
function update_lines() {
    var lfr = (Session.get('HQ') ? HQLineFrames.findOne({'frame': Session.get('frame')})
        : LineFrames.findOne({'frame': Session.get('frame')}));
    lines_src.setData(JSON.parse(lfr.gj));
}
function next_frame() {
    Session.set('frame', Math.min(Session.get('frame') + 1, MAX_FRAME));
}
function previous_frame() {
    Session.set('frame', Math.max(Session.get('frame') - 1, MIN_FRAME));
}

function fa_icon(name) {
    return '<i class="fa fa-' + name + '"></i> ';
}

function set_route_info(info, color, publ) {
    var a = new Date(info['stt']);
    //console.log(a.toDateString(), a.toLocaleString())
    var re = /[a-zA-Z- ]+, [A-Z]{2}/,
        m;
    var fr = info['fr'].replace(/, [0-9]{5}/g, ''),
        to = info['to'].replace(/, [0-9]{5}/g, ''),
        stt = a.toDateString().substr(0, 4).toLowerCase() +
            a.toLocaleString().replace(/\/[0-9]{4},/g, '').replace(/:00[ ]?/g, '').toLowerCase(),
        live = (color == 'green' ? 'live' : 'proposed'),
        publ = (publ == "0" ? 'private' : 'public');
    if ((m = re.exec(fr)) !== null) {
        if (m.index === re.lastIndex) {
            re.lastIndex++;
        }
        fr = m[0].replace(/(^\s+|\s+$)/g, '');
    }
    if ((m = re.exec(to)) !== null) {
        if (m.index === re.lastIndex) {
            re.lastIndex++;
        }
        to = m[0].replace(/(^\s+|\s+$)/g, '');
    }

    var content = live + ' ' + publ + ' route<br>' + fa_icon('angle-right') +
        fr + '<br>' + fa_icon('map-marker') + to +
        '<br>' + fa_icon('road') + info['dst'] + '<br>' + fa_icon('clock-o') + info['eta'] +
        '<br>' + fa_icon('calendar') + stt +
        '<br>' + fa_icon('user') + info['res'] + ' out of ' + info['cap'];
    Session.set('route_info', content);
}
function unset_route_info() {
    Session.set('route_info', false);
}

function toggle_color(color, state) {
    map.batch(function (batch) {
        color_layers[color].forEach(function (elem) {
            batch.setLayoutProperty(elem, 'visibility', (state ? 'visible' : 'none'));
        });
    });
}


var colorscale = {
    "red": '#f07a5f',
    "yellow": '#F0E65F',
    "green": '#7bc89d'
};
var linewidth_scale = {
    red: 5,
    yellow: 6.5,
    green: 8
};
var opacity_scale = {
    "green": 0.32,
    "yellow": 0.27,
    "red": 0.20
};
var opacity_anim_scale = {
    'appear': 0.3,
    'disappear': 0.2,
    'success': 0.6,
    'golive': 0.5,
};
var dasharray_anim = {
    '0': [3, 0.5],
    'appear': [3, 1],
    'disappear': [2, 2],
    'success': [4, 2],
    'golive': [4, 2]
};
var anim_list = ['0', 'appear', 'disappear', 'success', 'golive'];


// ANIMATIONS
function layers_visible_for_animation(anim) {
    return _.difference(animation_layers[anim], Session.get('invisible_layers'));
}

function run_animations() {
    var invisible_layers = _.flatten(_.map(Session.get('layer_vis'), function (val, colorname) {
        return (val ? [] : color_layers[colorname]);
    }));
    Session.set('invisible_layers', invisible_layers);

    //var layers_appearing = layers_visible_for_animation('appear');
    //map.batch(function (batch) {
    //    _.each(layers_appearing, function (layername) {
    //        batch.setLayoutProperty(layername, 'visibility', 'visible');
    //        batch.setPaintProperty(layername, 'line-opacity', 0.3);
    //    });
    //});

    var layers_success = layers_visible_for_animation('success');
    map.batch(function (batch) {
        _.each(layers_success, function (layername) {
            batch.setLayoutProperty(layername, 'visibility', 'visible');
            //batch.setPaintProperty(layername, 'line-opacity', 0.5);
            batch.setPaintProperty(layername, 'line-dasharray', [1,0]);
        });
    });
    setTimeout(function () {
        map.batch(function (batch) {
            _.each(layers_success, function (layername) {
                batch.setPaintProperty(layername, 'line-width', 100);
                batch.setPaintProperty(layername, 'line-opacity', 0);
            });
        });
    }, 160);
    setTimeout(function () {
        map.batch(function (batch) {
            _.each(layers_success, function (layername) {
                batch.setLayoutProperty(layername, 'visibility', 'none');
                batch.setPaintProperty(layername, 'line-opacity', 0.5);
                batch.setPaintProperty(layername, 'line-width', 8);
                batch.setPaintProperty(layername, 'line-dasharray', [4,2]);
            });
        });
    }, 360);

    //var layers_golive = layers_visible_for_animation('golive');
    //map.batch(function (batch) {
    //    _.each(layers_golive, function (layername) {
    //        batch.setPaintProperty(layername, 'line-opacity', 0.5);
    //        batch.setPaintProperty(layername, 'line-dasharray', [1,0]);
    //    });
    //});
    //setTimeout(function () {
    //    map.batch(function (batch) {
    //        _.each(layers_golive, function (layername) {
    //            batch.setPaintProperty(layername, 'line-width', 40);
    //            batch.setPaintProperty(layername, 'line-opacity', 0.2);
    //            batch.setPaintProperty(layername, 'line-color', colorscale['green']);
    //        });
    //    });
    //}, 120);
    //setTimeout(function () {
    //    map.batch(function (batch) {
    //        _.each(layers_golive, function (layername) {
    //            batch.setPaintProperty(layername, 'line-opacity', 0.5);
    //            batch.setPaintProperty(layername, 'line-width', 7);
    //            batch.setPaintProperty(layername, 'line-color', colorscale['yellow']);
    //            batch.setPaintProperty(layername, 'line-dasharray', [4,2]);
    //        });
    //    });
    //}, 120);


}

function prepare_before_next_slide() {
    var invisible_layers = Session.get('invisible_layers');

    map.batch(function (batch) {
        var layers_appearing = layers_visible_for_animation('appear');
        _.each(layers_appearing, function (layername) {
            batch.setLayoutProperty(layername, 'visibility', 'none');
            //batch.setPaintProperty(layername, 'line-width', 5);
            batch.setPaintProperty(layername, 'line-opacity', 0.0);
        });
    });
}

function restore_defaults_after_animations() {
    var ll, ml, invisible_layers = Session.get('invisible_layers');
    map.batch(function (batch) {
        _.each(colorscale, function (chex, kolor) {
            _.each(_.difference(anim_list, ["0"]), function (anim) { // animation type
                ll = "route." + kolor + '.' + anim;
                if (typeof _.find(invisible_layers, ll) == "undefined") {
                    batch.setLayoutProperty(ll, 'visibility', 'visible');
                    batch.setPaintProperty(ll, "line-color", colorscale[kolor]);
                    batch.setPaintProperty(ll, "line-width", linewidth_scale[kolor]);
                    batch.setPaintProperty(ll, "line-opacity", opacity_anim_scale[anim]);
                    batch.setPaintProperty(ll, "line-dasharray", dasharray_anim[anim]);
                }
                ml = "marker." + kolor + '.' + anim;
                if (typeof _.find(invisible_layers, ml) == "undefined") {
                    batch.setLayoutProperty(ml, 'visibility', 'visible');
                    batch.setPaintProperty(ml, "circle-color", colorscale[kolor]);
                    batch.setPaintProperty(ml, "circle-radius", linewidth_scale[kolor]);
                    batch.setPaintProperty(ml, "circle-opacity", opacity_anim_scale[anim]);
                }
            });
        });
    });
}


function date_from_frame(frame) {
    var dt = new Date(2015, 4, 23); // months start from 0, so May is 4
    dt.setDate(dt.getDate() + frame);
    //return dt.toDateString();
    var dts = dt.toDateString().substr(0, 3) + ' ' + (dt.getMonth() + 1) + '/' + (dt.getDate());
    return dts.toLowerCase();
}

//lets display the static output now
function updateTmlOutput(figure, activate) {
    var rfigure_tml, h_tml, v_tml;
    //if activate then add .active to #input-wrapper to help animate the #reel
    if (activate)
        $("#tml_wrapper").addClass("active");

    //because of the step param the slider will return values in multiple of 0.05 so we have to round it up
    rfigure_tml = Math.round(figure);
    //displaying the static output
    $("#static-output").html(date_from_frame(rfigure_tml));

    //positioning #static-output and #reel
    var proportion = (figure - MIN_FRAME) / (MAX_FRAME - MIN_FRAME);
    h_tml = (2 + proportion * ($("#tml_wrapper").width() - $("#reel").width())) + 'px';
    //vertical positioning of #rn
    v_tml = (rfigure_tml - MIN_FRAME) * $("#reel").height() * -1 + 'px';

    //applying the positions
    $("#static-output, #reel").css({left: h_tml});
    //#rn will be moved using transform+transitions for better animation performance. The false translateZ triggers GPU acceleration for better performance.
    $("#rn").css({transform: 'translateY(' + v_tml + ') translateZ(0)'});
}
function tml_deactivate() {
    //remove .active from #input-wrapper
    $("#tml_wrapper").removeClass("active");
}